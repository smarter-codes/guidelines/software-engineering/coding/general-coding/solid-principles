> A computer program is a collection of data and instructions that can be executed by a computer to perform a specific task.  ~[Computer Program, Wikipedia](https://en.wikipedia.org/wiki/Computer_program)

Ironically, we as developers need to write code that other humans can easily understand and is easy to extend/maintain. Imagine all of us lived in an identical houses, with everything being in the same place, in everyones house. I could invite someone over and there is no need to guide them on how the doors open, or where they can find a fork. But sometimes, you try to light the stove and there's heat coming out of the AC, well you could technically cook there, but someone who doesn't know that could set the whole house on fire.

Basically, we need to write code that others can understand fast, by writing clean-clear code and abstracting away lines of code someone else does not need to read, while modifying a certain part of the app.

# Revisiting SOLID Principles

- [Uncle Bob](https://en.wikipedia.org/wiki/Robert_C._Martin)s [talk](https://www.youtube.com/watch?v=zHiWqnTWsn4) is awesome
- His book, [Clean Code](https://www.amazon.in/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)(he has several others), for those who prefer that. 
- Another [nice video](https://www.youtube.com/watch?v=rtmFCcjEgEw), with some more practical examples

# Issues for Each Principle
- [**Single Responsibility Principle**](https://gitlab.com/smarter-codes/guidelines/software-engineering/coding/general-coding/solid-principles/-/issues/1)


# Design Patterns

# Using Boilerplates
